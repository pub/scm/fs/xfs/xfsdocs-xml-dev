<?xml version='1.0' encoding='utf-8' ?>
<!DOCTYPE chapter PUBLIC "-//OASIS//DTD DocBook XML V4.4//EN" "http://www.oasis-open.org/docbook/xml/4.4/docbookx.dtd" [
]>
<chapter id="xfs-quotas-lab">
	<title>XFS Practical Exercises - 05- Quotas</title>
	<section id="Overview">
		<title>Overview</title>
		<section id="Goals">
			<title>Goals</title>
			<para>The goal of this lab to learn how to use and interpret the XFS quota commands.</para>
		</section>
		<section id="Prerequisites">
			<title>Prerequisites</title>
			<para>The user needs to understand filesystems administration including mkfs and mount.</para>
		</section>
		<section id="Setup">
			<title>Setup</title>
			<para>An empty filesystem is needed for the lab.</para>
		</section>
	</section>
	<section id="Exercises">
		<title>Exercises</title>
		<section id="Exercise1">
			<title>Exercise 1 - Configuring Quotas and Quota Reporting</title>
			<orderedlist>
			<listitem>
				<para>Create an entry for this filesystem in fstab, enable user quotas by adding quota 
					to the list of mount options and mount the filesystem.</para>
				<para><programlisting>
# sudo mkdir /mnt/xfstest
# sudo vi /etc/fstab
/dev/hdb1 /mnt/xfstest xfs defaults,quota 0 0
# sudo mount /mnt/xfstest
				</programlisting></para>
			</listitem>
			<listitem>
				<para>Investigate the following xfs_quota administrative commands</para>
				<para><programlisting>
# sudo /usr/sbin/xfs_quota -x
xfs_quota> help
xfs_quota> report
xfs_quota> state
xfs_quota> path
				</programlisting></para>
			</listitem>
			<listitem>
				<para>Create some files on the filesystem and rerun the report command.</para>
			</listitem>
			</orderedlist>
		</section>
		<section id="Exercise2">
			<title>Exercise 2 - Quota Controls (user/group)</title>
			<orderedlist>
			<listitem>
				<para>un “xfs_quota -x” as root and set quota controls on an ordinary user. </para>
				<para><programlisting>
# sudo /usr/sbin/xfs_quota -x /mnt/xfstest
xfs_quota> limit bsoft=10m bhard=20m youruser
				</programlisting></para>
			</listitem>
			<listitem>
				<para>Investigate how the limit command has affected your user.</para>
				<para><programlisting>
# /usr/sbin/xfs_quota -c 'quota -v'
				</programlisting></para>
			</listitem>
			<listitem>
				<para>Exceed the soft quota. Note that while your (soft) quota has been exceeded you 
					can still write files.</para>
				<para><programlisting>
# cd /mnt/xfstest
# dd if=/dev/urandom of=./testfile1 bs=1k count=15000
# /usr/sbin/xfs_quota -c 'quota' /mnt/xfstest
# ls > testfile2
				</programlisting></para>
			</listitem>
			<listitem>
				<para>Exceeding hard Quota</para>
				<para><programlisting>
# rm testfile1 testfile2
# dd if=/dev/urandom of=./testfile1 bs=1k count=30000
dd: writing `./testfile': Disk quota exceeded
20417+0 records in
20416+0 records out
20905984 bytes (21 MB) copied, 4.20713 seconds, 5.0 MB/s
				</programlisting></para>
			</listitem>
			<listitem>
				<para>Run the quota command and examine the output.</para>
				<para><programlisting>
# /usr/sbin/xfs_quota -c quota
Disk quotas for User youruser (500)
Filesystem              Blocks      Quota      Limit  Warn/Time      Mounted on
/dev/hdb1                20416      10240      20480   00  [6 days] /mnt/xfstest
				</programlisting></para>
			</listitem>
			<listitem>
				<para>Run the generic repquota command to compare the behavior.</para>
				<para><programlisting>
# /usr/sbin/repquota /mnt/xfstest
*** Report for user quotas on device /dev/hdb1
Block grace time: 7days; Inode grace time: 7days
                        Block limits                File limits
User            used    soft    hard  grace    used  soft  hard  grace
----------------------------------------------------------------------
youruser   +-   20416   10240   20480  6days       1     0     0
				</programlisting></para>
			</listitem>
			<listitem>
				<para>Remove the test files created above and investigate the affects of quotas on 
					holey files. Holey files can be created with dd </para>
				<para><programlisting>
# dd if=/dev/urandom of=./testfile bs=1k count=1 seek=2000000
				</programlisting></para>
			</listitem>
			<listitem>
				<para>Compare the outputs of</para>
				<para><programlisting>
# ls -hl
# du -h *
# /usr/sbin/xfs_quota -c quota
				</programlisting></para>
			</listitem>
			<listitem>
				<para>Experiment with inode quotas.</para>
				<para><programlisting>
xfs_quota> limit isoft=5 ihard=10 youruser
				</programlisting></para>
			</listitem>
			<listitem>
				<para>Create files to exceed your soft and hard limits. Note that the xfs_quota quota 
					command takes a -i option to report on inodes.</para>
			</listitem>
			<listitem>
				<para>Experiment with group quotas. You will need to remount the filesystem after 
					adding the gquota option to the fstab. xfs_quota commands use -g to 
					indicate they are working with groups.</para>
			</listitem>
			</orderedlist>
		</section>
		<section id="Exercise3">
			<title>Exercise 3 - Quota Controls (project)</title>
			<orderedlist>
			<listitem>
				<para>Add pquota to the mount options for your test file system. You will have to 
					remove group quotas if set as they are not compatible with group project quotas.</para>
			</listitem>
			<listitem>
				<para>Create target directories</para>
				<para><programlisting>
# mkdir /mnt/xfstest/a /mnt/xfstest/b
				</programlisting></para>
			</listitem>
			<listitem>
				<para>Create /etc/projects</para>
				<para><programlisting>
33:/mnt/xfstest/a
33:/mnt/xfstest/b
				</programlisting></para>
			</listitem>
			<listitem>
				<para>Create etc/projid</para>
				<para><programlisting>
testproject:33
				</programlisting></para>
			</listitem>
			<listitem>
				<para>Update the projects state and set project limits.</para>
				<para><programlisting>
# sudo /usr/sbin/xfs_quota -x /mnt/xfstest
xfs_quota> project -s testproject
xfs_quota> print
Filesystem          Pathname
/mnt/xfstest    /dev/hdb1 (pquota)
/mnt/xfstest/a /dev/hdb1 (project 33, testproject)
/mnt/xfstest/b /dev/hdb1 (project 33, testproject)
xfs_quota> limit -p bsoft=10m bhard=20m testproject
xfs_quota> quota -vp testproject
Disk quotas for Project #33 (testproject)
Filesystem         Blocks      Quota      Limit  Warn/Time      Mounted on
/dev/hdb1            0         10240      20480   00 [--------] /mnt/xfstest
				</programlisting></para>
			</listitem>
			</orderedlist>
		</section>
		<section id="Exercise4">
			<title>Exercise 4 - Examining Quota Internals</title>
			<para>Examine quota inodes and quota entries.</para>
			<orderedlist>
			<listitem>
				<para>Examine the quota inodes.</para>
				<para><programlisting>
# xfs_db -xr /dev/hdb1
xfs_db: sb 0
xfs_db: p
...
uquotino = null
pquotino = 132

xfs_db: inode 132
xfs_db: p
...
				</programlisting></para>
			</listitem>
			<listitem>
				<para>Examine the quota entries.</para>
				<para><programlisting>
xfs_db: dquot -p testproject
xfs_db: p
diskdq.magic = 0x4451
diskdq.version = 0x1
diskdq.flags = 0x2
diskdq.id = 33
diskdq.blk_hardlimit = 2560
diskdq.blk_softlimit = 1280
diskdq.ino_hardlimit = 0
diskdq.ino_softlimit = 0
diskdq.bcount = 1
diskdq.icount = 2
diskdq.itimer = 0
diskdq.btimer = 0
diskdq.iwarns = 0
diskdq.bwarns = 0
diskdq.rtb_hardlimit = 0
diskdq.rtb_softlimit = 0
diskdq.rtbcount = 0
diskdq.rtbtimer = 0
diskdq.rtbwarns = 0
				</programlisting></para>
			</listitem>
			<listitem>
				<para>Examine inodes of quota controlled files/directories</para>
				<para><programlisting>
# cd /mnt/xfstest/a
# ls > testfile
# ls -ia
  133 .     128 ..    135 testfile

# sudo xfs_db -xr /dev/hdb1
xfs_db: inode 133
xfs_db: p
...
core.projid = 33
core.uid = 0
core.gid = 0
...
				</programlisting></para>
			</listitem>
			</orderedlist>
		</section>
	</section>
	<section id="Questions">
		<title>Questions</title>
		<orderedlist>
		<listitem>
			<para>How would you inform users of their quota violations?</para>
		</listitem>
		</orderedlist>
	</section>
	<section id="Answers">
		<title>Answers</title>
		<orderedlist>
		<listitem>
			<para>How would you inform users of their quota violations?
			<itemizedlist>
			<listitem><para>Email users who are over quota. The generic quota package provides warnquota which is 
					usually executed daily using cron.</para></listitem>
			<listitem><para>For interactive users quota commands may be added to shell startup scripts 
					(ie /etc/bash.bashrc.local).</para></listitem>
			<listitem><para>Generate a quota report on the user or departments homepage.</para></listitem>
			</itemizedlist>
			</para>
		</listitem>
		</orderedlist>
	</section>
</chapter>
