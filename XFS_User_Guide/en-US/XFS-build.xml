<?xml version='1.0' encoding='utf-8' ?>
<!DOCTYPE chapter PUBLIC "-//OASIS//DTD DocBook XML V4.4//EN" "http://www.oasis-open.org/docbook/xml/4.4/docbookx.dtd" [
]>
<chapter id="xfs-build">
	<title>XFS Build</title>
	<section>
		<title>Building XFS</title>
		<para>This tutorial describes how to</para>
		<itemizedlist>
		<listitem><para>obtain the latest XFS source code</para></listitem>
		<listitem><para>how to build and install the commands and kernel components</para></listitem>
		</itemizedlist>
	</section>
	<section>
		<title>XFS Source Code</title>
		<para>Latest XFS is available via git from oss.sgi.com:</para>
		<para><programlisting>
> git clone git://oss.sgi.com/xfs/xfs.git
...
> git clone git://oss.sgi.com/xfs/xfsprogs.git
...
> git clone git://oss.sgi.com/xfs/xfsdump.git
...
</programlisting></para>
	</section>
	<section>
		<title>Building xfs-cmds</title>
		<para>Building the XFS Userspace</para>
		<para><programlisting>
> xfsprogs
> make
...</programlisting></para>
		<para>RPM packages will be placed in RPMS/&lt;arch&gt;</para>
		<para><programlisting>
> cd RPMS/x86_64
> sudo rpm -i xfsprogs-2.8.15-1.x86_64.rpm
> sudo rpm -i xfsdump-2.2.43-1.x86_64.rpm
</programlisting></para>
		<para>Source RPM packages will be placed in SRPMS/</para>
		<note><para>Distribution-provided packages are generally preferable to
			    those generated from this tree, for OS consistency</para></note>
	</section>
	<section>
		<title>XFS Userspace Packaging</title>
		<para>XFS uses autoconf/configure</para>
		<para>Includes internationalization support</para>
		<para>User space packages</para>
		<itemizedlist>
		<listitem><para>xfsprogs
			<itemizedlist>
			<listitem><para>libhandle, libxfs, libxlog, libdisk</para></listitem>
			</itemizedlist>
		</para></listitem>
		<listitem><para>xfsdump
			<itemizedlist>
			<listitem><para>dependency on attr packages</para></listitem>
			<listitem><para>librmt</para></listitem>
			</itemizedlist>
		</para></listitem>
		</itemizedlist>
	</section>
	<section>
		<title>Kernel Configuration Options</title>
		<para>CONFIG_XFS_FS</para>
		<itemizedlist>
		<listitem><para>XFS can be built as a module or statically linked into the kernel</para></listitem>
		</itemizedlist>
		<para>CONFIG_XFS_QUOTA</para>
		<itemizedlist>
		<listitem><para>support for quota disk limits.</para></listitem>
		</itemizedlist>
		<para>CONFIG_XFS_POSIX_ACL</para>
		<itemizedlist>
		<listitem><para>support for Access Control Lists.</para></listitem>
		</itemizedlist>
		<para>CONFIG_XFS_RT</para>
		<itemizedlist>
		<listitem><para>support for the realtime allocator that provides deterministic data rates.</para></listitem>
		</itemizedlist>
		<para>CONFIG_XFS_DEBUG</para>
		<itemizedlist>
		<listitem><para>include debugging features in the build (ie ASSERTs, sanity checking code, etc).</para></listitem>
		</itemizedlist>
		<warning><para>A debug XFS build is not recommended for production use - some error conditions
			       will panic the box to aid debugging.</para></warning>
	</section>
	<section>
		<title>Build Kernel Components</title>
		<para>Configure and build kernel and modules</para>
		<para><programlisting>
> cd xfs
> make xconfig
> make bzImage
> make modules
> sudo make modules_install</programlisting></para>
		<para>If XFS wasn't built as a module</para>
		<para><programlisting>
# cp arch/&lt;arch&gt;/boot/bzImage /boot/vmlinuz
# cp System.map /boot/System.map
# vim /boot/grub/menu.lst
# reboot</programlisting></para>
	</section>
	<section>
		<title>Load XFS Module</title>
		<para>Load the XFS module</para>
		<para><programlisting>
# modprobe xfs
# lsmod
Module                Size Used by
xfs                 848864 0</programlisting></para>
		<para>Can now mount XFS filesystems...</para>
	</section>
	<section>
		<title>Other products that know XFS</title>
		<para>Quota (sourceforge.net)</para>
		<itemizedlist>
		<listitem><para>generic quota commands know about XFS</para></listitem>
		<listitem><para>do not understand project quotas
			<itemizedlist>
			<listitem><para>seen as too intrusive, XFS only</para></listitem>
			</itemizedlist>
		</para></listitem>
		</itemizedlist>
		<para>mount (kernel.org)</para>
		<itemizedlist>
		<listitem><para>from <emphasis>util-linux</emphasis> package</para></listitem>
		<listitem><para>knows where the primary XFS SB is</para></listitem>
		<listitem><para>knows how to extract the label and UUID
			<itemizedlist>
			<listitem><para>for UUID= and LABEL= mounts</para></listitem>
			</itemizedlist>
		</para></listitem>
		</itemizedlist>
		<para>Samba (samba.org)</para>
		<itemizedlist>
		<listitem><para>has some XFS specific code in its handling of quotas
			<itemizedlist>
			<listitem><para>uses XFS quotactl(2) calls</para></listitem>
			</itemizedlist>
		</para></listitem>
		</itemizedlist>
	</section>
</chapter>

