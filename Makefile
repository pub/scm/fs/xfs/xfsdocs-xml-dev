

SUBDIRS = XFS_Filesystem_Structure XFS_Labs XFS_User_Guide

html:  $(addsuffix -html,$(SUBDIRS))
html-single:  $(addsuffix -html-single,$(SUBDIRS))
pdf:  $(addsuffix -pdf,$(SUBDIRS))

clean: $(addsuffix -clean,$(SUBDIRS))

%-html:
	$(MAKE) -C $* publican-html

%-html-single:
	$(MAKE) -C $* publican-html-single

%-pdf:
	$(MAKE) -C $* publican-pdf

%-clean:
	$(MAKE) -C $* clean
